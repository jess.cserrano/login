package com.example.proyectologin.data.repository
import com.example.proyectologin.data.model.LoginUserRequest
import com.example.proyectologin.data.model.UserAccessResponse

class LoginApiRepository {

    fun login(loginUserRequest: LoginUserRequest): UserAccessResponse? {

        val userAccess = LoginApi.retrofitService.login(loginUserRequest)

        val response = userAccess.execute()
        return response.body()
    }
}