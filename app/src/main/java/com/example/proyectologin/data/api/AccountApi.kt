package com.example.proyectologin.data.api

import com.example.proyectologin.data.model.AccountItemResponse
import com.example.proyectologin.data.network.MyService
import retrofit2.Call
import retrofit2.http.GET

interface AccountsInterface {

    @GET("accounts")
    fun getAccounts():Call<List<AccountItemResponse>>

}

var retrofit = MyService().getRetrofit()

object AccountsApi {
    val accountsRetrofitService: AccountsInterface by lazy {
        retrofit.create(AccountsInterface::class.java)
    }
}
