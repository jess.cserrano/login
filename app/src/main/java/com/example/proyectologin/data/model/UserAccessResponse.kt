package com.example.proyectologin.data.model


import com.example.proyectologin.domain.UserAccess
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserAccessResponse(
    @Json(name = "httpCode")
    val httpCode: String,
    @Json(name = "httpMessage")
    val httpMessage: String,
    @Json(name = "moreInformation")
    val moreInformation: String
)

internal fun UserAccessResponse.toEntity() = UserAccess(httpCode, httpMessage, moreInformation)