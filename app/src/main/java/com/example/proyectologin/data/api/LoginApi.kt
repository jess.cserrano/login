import com.example.proyectologin.data.model.LoginUserRequest
import com.example.proyectologin.data.model.UserAccessResponse
import com.example.proyectologin.data.network.MyService
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface LoginApiService {

    @Headers("Accept: application/json", "Content-Type: application/json")

    @POST("login")
    fun login(
        @Body request: LoginUserRequest
    ): Call<UserAccessResponse>

}

var retrofit = MyService().getRetrofit()

object LoginApi {

    val retrofitService: LoginApiService by lazy {
        retrofit.create(LoginApiService::class.java)
    }

}