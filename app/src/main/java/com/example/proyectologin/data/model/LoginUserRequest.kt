package com.example.proyectologin.data.model


import com.example.proyectologin.domain.LoginUser
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class LoginUserRequest(
    @Json(name = "password")
    val password: String,
    @Json(name = "user")
    val user: String
)

internal fun LoginUser.toRequest() = LoginUserRequest(password,user)