package com.example.proyectologin.data.model

import com.example.proyectologin.domain.Accounts
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class AccountItemResponse(
    @Json(name = "kind")
    val kind: String,
    @Json(name = "products")
    val products: List<Product>
) {
    @JsonClass(generateAdapter = true)
    data class Product(
        @Json(name = "iban")
        val iban: String?,
        @Json(name = "pan")
        val pan: String?,
        @Json(name = "name")
        val name: String,
        @Json(name = "productId")
        val productId: String
    )

}

internal fun List<AccountItemResponse>.toEntity() = Accounts(
    this.mapNotNull { item ->
        runCatching {
            Accounts.AccountsItem(
                item.kind,
                item.products.map { product ->
                    Accounts.AccountsItem.Product(
                        product.iban,
                        product.pan,
                        product.name,
                        product.productId
                    )
                }
            )
        }.getOrNull()
    }
)


/*
internal fun AccountsResponse.toEntity() = Accounts(
    accounts.mapNotNull {item->
        runCatching {
            Accounts.AccountsItem(
                item.kind,
                item.products.mapNotNull { product ->
                    Accounts.AccountsItem.Product(
                        product.iban,
                        product.name,
                        product.productId
                    )
                }
            )
        }.getOrNull()
    }
)*/
