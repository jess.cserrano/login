package com.example.proyectologin.data.repository

import com.example.proyectologin.data.api.AccountsApi
import com.example.proyectologin.data.model.AccountItemResponse

class AccountRepository {

    fun getAccounts(): List<AccountItemResponse>? {

        val accountsRetro = AccountsApi.accountsRetrofitService.getAccounts()

        val response = accountsRetro.execute()

        return response.body()

    }
}