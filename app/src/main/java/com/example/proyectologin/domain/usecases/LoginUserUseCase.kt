package com.example.proyectologin.domain.usecases

import com.example.proyectologin.data.model.toEntity
import com.example.proyectologin.data.model.toRequest
import com.example.proyectologin.data.repository.LoginApiRepository
import com.example.proyectologin.domain.LoginUser
import com.example.proyectologin.domain.UserAccess
import kotlinx.coroutines.withContext
import kotlin.coroutines.coroutineContext

class LoginUserUseCase(private val loginApiRepository: LoginApiRepository) {

    suspend fun execute(loginUser: LoginUser): UserAccess? {

      return withContext(coroutineContext) {
             loginApiRepository.login(loginUser.toRequest())?.toEntity()
        }

    }
}