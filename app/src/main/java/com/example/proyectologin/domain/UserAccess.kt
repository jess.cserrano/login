package com.example.proyectologin.domain

data class UserAccess(val httpCode: String, val httpMessage: String, val moreInformation: String)
