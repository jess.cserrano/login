package com.example.proyectologin.domain


@JvmInline
value class Accounts(val accounts: List<AccountsItem>) {

    data class AccountsItem(
        val kind: String,
        val products: List<Product>
    ) {
        data class Product(
            val iban: String?,
            val pan: String?,
            val name: String,
            val productId: String
        )

    }

}