package com.example.proyectologin.domain.usecases

import com.example.proyectologin.data.model.toEntity
import com.example.proyectologin.data.repository.AccountRepository
import com.example.proyectologin.domain.Accounts
import kotlinx.coroutines.withContext
import kotlin.coroutines.coroutineContext

class GetAccountsUseCase(private val accountRepository: AccountRepository) {

    suspend fun execute(): Accounts? {

        return withContext(coroutineContext) {
            accountRepository.getAccounts()?.toEntity()
        }
    }
}