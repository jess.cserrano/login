package com.example.proyectologin.domain

data class LoginUser(var password: String, var user: String)
