package com.example.proyectologin.presentation.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.proyectologin.databinding.ActivityMainBinding
import com.example.proyectologin.data.repository.LoginApiRepository
import com.example.proyectologin.domain.LoginUser
import com.example.proyectologin.domain.usecases.LoginUserUseCase
import com.example.proyectologin.presentation.viewmodel.LoginViewModel
import com.example.proyectologin.presentation.viewmodel.ViewModelFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loginViewModel = ViewModelProvider(this, ViewModelFactory(this))[LoginViewModel::class.java]

        val btnAccess = binding.btnAccess
        val user = binding.editTextUser
        val password = binding.editTextPassword


        loginViewModel.userAccessData.observe(this, Observer { data ->
            if (data != null) {
                Toast.makeText(this, data.httpMessage, Toast.LENGTH_SHORT).show()

                val intent = Intent(this, WelcomeActivity::class.java)
                this.startActivity(intent)
            } else {
                Toast.makeText(this, "acceso denegado", Toast.LENGTH_SHORT).show()
            }
        })

        btnAccess.setOnClickListener(View.OnClickListener {

            val loginUser = LoginUser(password.text.toString(), user.text.toString())

            GlobalScope.launch {
                loginViewModel.login(loginUser)
            }

        })

    }
}