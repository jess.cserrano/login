package com.example.proyectologin.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.proyectologin.domain.Accounts
import com.example.proyectologin.domain.usecases.GetAccountsUseCase

class AccountsViewModel(
    private val getAccountsUseCase: GetAccountsUseCase
) : ViewModel() {

    val accountsData = MutableLiveData<Accounts?>()

    suspend fun getAccounts(){
        accountsData.postValue(getAccountsUseCase.execute())
    }

}