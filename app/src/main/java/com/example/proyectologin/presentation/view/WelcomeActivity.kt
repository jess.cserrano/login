package com.example.proyectologin.presentation.view

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.proyectologin.databinding.ActivityWelcomeBinding
import com.example.proyectologin.presentation.viewmodel.AccountsViewModel
import com.example.proyectologin.presentation.viewmodel.ViewModelFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class WelcomeActivity : AppCompatActivity() {
    @SuppressLint("SetTextI18n")

    private lateinit var binding: ActivityWelcomeBinding

    private lateinit var accountsViewModel: AccountsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        accountsViewModel =
            ViewModelProvider(this, ViewModelFactory(this))[AccountsViewModel::class.java]

        binding.textViewWelcome

        accountsViewModel.accountsData.observe(this, Observer { data ->

            if (data != null) {
                Toast.makeText(this, (data.accounts.size).toString(), Toast.LENGTH_SHORT).show()
                var intent = Intent(this,AccountsActivity::class.java)
                startActivity(intent)

            } else {
                Toast.makeText(this, "NOO OK , KO", Toast.LENGTH_SHORT).show()

            }
        })

        binding.btnGetAccounts.setOnClickListener(View.OnClickListener {

            GlobalScope.launch {
                accountsViewModel.getAccounts()
            }

        })

    }
}
