package com.example.proyectologin.presentation.viewmodel

import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.example.proyectologin.data.repository.AccountRepository
import com.example.proyectologin.data.repository.LoginApiRepository
import com.example.proyectologin.domain.usecases.GetAccountsUseCase
import com.example.proyectologin.domain.usecases.LoginUserUseCase


class ViewModelFactory(saveStateOwner: SavedStateRegistryOwner) :
    AbstractSavedStateViewModelFactory(saveStateOwner, null) {
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        return when {
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> {
                val repo = LoginApiRepository()
                val loginUserUseCase = LoginUserUseCase(repo)
                LoginViewModel(loginUserUseCase)
            }

            modelClass.isAssignableFrom(AccountsViewModel::class.java) -> {
                val repoAccounts = AccountRepository()
                val getAccountsUseCase = GetAccountsUseCase(repoAccounts)
                AccountsViewModel(getAccountsUseCase)
            }
            else -> {
                throw IllegalArgumentException("Unknown ViewModel class")
            }
        } as T
    }
}

