package com.example.proyectologin.presentation.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.proyectologin.databinding.ItemCardBinding
import com.example.proyectologin.domain.Accounts

class AccountAdapter(private val listAccounts: List<Accounts.AccountsItem>) :
    RecyclerView.Adapter<AccountAdapter.ViewHolder>() {


    class ViewHolder(private val binding: ItemCardBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Accounts.AccountsItem) {

            for (i in item.products) {
                binding.tvNameAccount.text = i.name

                if (i.pan != null) {
                    binding.tvAccount.text = i.pan
                } else {
                    binding.tvAccount.text = i.iban
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        val listItemBinding = ItemCardBinding.inflate(inflater, parent, false)
        return ViewHolder(listItemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(listAccounts[position])

        /*for (i in listAccounts) {

            for (x in i.products) {
                holder.binding.tvNameAccount.text = x.name

                if (x.iban != null) {
                    holder.binding.tvAccount.text = x.iban
                } else {
                    holder.binding.tvAccount.text = x.pan
                }
            }
        }*/


    }

    override fun getItemCount(): Int = listAccounts.size

}
