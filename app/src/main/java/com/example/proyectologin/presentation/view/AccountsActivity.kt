package com.example.proyectologin.presentation.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.proyectologin.databinding.ActivityAccountsBinding
import com.example.proyectologin.presentation.viewmodel.AccountsViewModel
import com.example.proyectologin.presentation.viewmodel.ViewModelFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AccountsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAccountsBinding
    private lateinit var accountsViewModel: AccountsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountsBinding.inflate(layoutInflater)
        setContentView(binding.root)



        accountsViewModel =
            ViewModelProvider(this, ViewModelFactory(this))[AccountsViewModel::class.java]

        accountsViewModel.accountsData.observe(this, Observer { data ->


            if (data != null) {
                Toast.makeText(this, (data.accounts.size).toString(), Toast.LENGTH_SHORT).show()

                val recyclerView = binding.rvAccounts
                val adapter = AccountAdapter(data.accounts)
                recyclerView.layoutManager = LinearLayoutManager(this)
                recyclerView.adapter = adapter

            } else {
                Toast.makeText(this, "Acceso denegado", Toast.LENGTH_SHORT).show()

            }
        })

        GlobalScope.launch {
            accountsViewModel.getAccounts()
        }
    }
}