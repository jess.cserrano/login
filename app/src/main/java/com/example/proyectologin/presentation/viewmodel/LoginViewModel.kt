package com.example.proyectologin.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.proyectologin.domain.LoginUser
import com.example.proyectologin.domain.UserAccess
import com.example.proyectologin.domain.usecases.LoginUserUseCase

class LoginViewModel(
    private val loginUserUseCase: LoginUserUseCase
) : ViewModel() {

    val userAccessData = MutableLiveData<UserAccess?>()

    suspend fun login(loginUser: LoginUser) {
        userAccessData.postValue(loginUserUseCase.execute(loginUser))
    }

}